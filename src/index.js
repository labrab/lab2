const mysql = require('mysql2/promise');
const fs = require('fs')
async function main () {
  const connection = await mysql.createConnection({
    host: process.env.DBHOST,
    user: process.env.DBUSER,
    password: process.env.DBPASSWORD,
    port: process.env.DBPORT
  });

  try {
    await connection.query('USE ' + process.env.DBNAME)
    const [rows] = await connection.query('SELECT * FROM students')
    const data = rows.sort((a, b) => new Date(a.birthday.replace('.', '-')) - new Date(b.birthday.replace('.', '-')));

    const first = data[0]
    const second = data.pop()

    let res = ''
    res += `Birthday: ${first.birthday} Full name: ${first.full_name}\n`
    res += `Birthday: ${second.birthday} Full name: ${second.full_name}\n`

    console.log(res)
    fs.writeFileSync('./result/data.txt', res, { encoding: 'utf8' })
  } catch (e) {
    console.log('Error', e)
  } finally {
    connection.end()
  }
}

main()
