# Students list

Run:

```bash
# Install dependencies
npm ci

# Run app
node start
```

`.env`:

```dotenv
DBPORT=
DBHOST=
DBNAME=
DBUSER=
DBPASSWORD=
```

With docker:

- Goto project dir using `cd`
- Run `docker-compose up -d --build`
